from flask import *
from codeAPI import myoswrap
from codeAPI.customExceptions import *

app = Flask('TP2 API')


def getUserList():
	users = []
	for line in myoswrap.readFileByLine('/etc/passwd'):
		users.append(line.split(':')[0])
	return users

INITIAL_USERS = [
	"Debian-exim",
	"root", 
    "daemon", 
    "bin", 
    "sys", 
    "sync", 
    "games", 
    "man", 
    "lp", 
    "mail", 
    "news", 
    "uucp", 
    "proxy", 
    "www-data", 
    "backup", 
    "list", 
    "irc", 
    "gnats", 
    "nobody", 
    "_apt"
]

def isInitialUser(username):
	if username in INITIAL_USERS:
		return True
	return False

#Done classical method to back related route (unit test this)
def hello():
	return "Welcome to Emma's API!"

#Done route method (test with deployment tests)
@app.route('/')
def route_hello(): #pragma: no cover
	return jsonify({'code':'2000', 'msg':hello()})


#classical method to back related route
def getUsers():
	users = getUserList()
	newUsers = []
	for user in users:
		if isInitialUser(user) is False:
			newUsers.append(user)
	
	userDict = {
		"InitialUsers": INITIAL_USERS,
		"NewUsers": newUsers
	}

	return userDict

#route method
@app.route('/getusers')
def route_getUsers(): #pragma: no cover
	return jsonify({'code':'2000', 'msg':getUsers()})


#done classical method to back related route
def delUser(username): 
	if username in getUserList():
		if isInitialUser(username):
			raise InitialUserException('cannot delete an initial user.')

		else:
			myoswrap.runCommandToRemoveUser(username)
			return 'deleted ' + username
	raise NonExistingUserException('The specified user (' + username + ') does not exist, cannot perform delete operation.')

#done route method
@app.route('/deluser')
def route_delUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = delUser(username)
			return jsonify({'code':'2000', 'msg':rep})
		except InitialUserException as iue:
			return make_response(jsonify({'code':'1003', 'msg':str(iue)}), 400)
		except NonExistingUserException as neue:
			return make_response(jsonify({'code':'1002', 'msg':str(neue)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory for deletion.'}), 400)



#classical method to back related route
def resetUsers():
	users = getUserList()
	deletioncount = 0

	for user in users:
		if not isInitialUser(user):
			myoswrap.runCommandToRemoveUser(user)
			deletioncount = deletioncount + 1

	return deletioncount

#route method 
@app.route('/resetusers')
def route_resetUsers(): #pragma: no cover
	count = resetUsers()
	if count == 0:
		return jsonify({'code':'2000', 'msg':'There were no users to delete.'})
	return jsonify({'code':'2000', 'msg':"Successfully deleted " + str(count) + " users."})



#classical method to back related route
def addUser(username):
	if username not in getUserList():
		myoswrap.runCommandToAddUser(username)
		return 'Successfully added ' + username
	raise AlreadyExistingUserException('cannot add a user that already exists.')

#route method 
@app.route('/adduser')
def route_addUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = addUser(username)
			return jsonify({'code':'2000', 'msg':rep})
		except AlreadyExistingUserException as aeue:
			return make_response(jsonify({'code':'1001', 'msg':str(aeue)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory in order to add new user.'}), 400)




#classical method to back related route
def getLogs():
	return myoswrap.readFileByLine('/var/log/auth.log')

#route method 
@app.route('/getlog')
def route_getLogs(): #pragma: no cover
	return jsonify({'code':'2000', 'msg':getLogs()})




if __name__ == "__main__": #pragma: no cover
	app.run(debug=True, host='0.0.0.0', port=5555)
