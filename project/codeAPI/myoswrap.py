import subprocess

def runCommandToAddUser(username):
	subprocess.run(["useradd", username])

def runCommandToRemoveUser(username):
	subprocess.run(["userdel", username])

def readFileByLine(filename):
	content = []
	f = open(filename, "r")
	while True:
		line = f.readline()

		if not line:
			break

		content.append(line)
	
	f.close()

	return content