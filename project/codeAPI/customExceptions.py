class InitialUserException(Exception):
        pass

class NonExistingUserException(Exception):
        pass

class AlreadyExistingUserException(Exception):
        pass