import unittest
import json
import unittest.mock

from codeAPI.customExceptions import *
from codeAPI import userAPI


class BasicTests(unittest.TestCase):

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_hello(self, mock_oswrap):
		actual = userAPI.hello()
		self.assertIn('Welcome', actual)
		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
		obj = userAPI.delUser('Roy')
		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Roy')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUserNonExisting(self, mock_oswrap):
		with self.assertRaises(NonExistingUserException) as context:
			userAPI.delUser('Brodeur')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delInitialUser(self, mock_oswrap):
		with self.assertRaises(InitialUserException) as context:
			#mocking a return value for one method (so the system appears to have 1 users overall)
			mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
			userAPI.delUser('root')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_isInitialUser(self, mock_oswrap):
		result = userAPI.isInitialUser('root')

		self.assertEqual(result, True)
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_isNotInitialUser(self, mock_oswrap):
		result = userAPI.isInitialUser('BananaSplit')

		self.assertEqual(result, False)

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUserList(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']

		obj = userAPI.getUserList()

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
	
	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getLogs(self, mock_oswrap):
		value = "Mar 15 23:41:53 1173ecb2be79 useradd[129]: new group: name=Blblbl, GID=1001"
		mock_oswrap.readFileByLine.return_value = [value]

		obj = userAPI.getLogs()
		mock_oswrap.readFileByLine.assert_called_with('/var/log/auth.log')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersIfNoNewUsers(self, mock_oswrap):
		obj = userAPI.getUserList()

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
	
	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersIfNewUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']

		obj = userAPI.getUserList()

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		
	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsersIfNewUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']

		obj = userAPI.resetUsers()

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Roy')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		self.assertEqual(obj, 1)
	
	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsersIfOnlyInitialUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
			
		obj = userAPI.resetUsers()

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()
		self.assertEqual(obj, 0)

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsersIfNoUser(self, mock_oswrap):
		obj = userAPI.resetUsers()

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()
		self.assertEqual(obj, 0)

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUserWhenNoDuplicate(self, mock_oswrap):
		obj = userAPI.addUser('Bob')

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_called_with('Bob')

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUserWhenDuplicate(self, mock_oswrap):
		with self.assertRaises(AlreadyExistingUserException) as context:
			mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']

			obj = userAPI.addUser('Roy')

			mock_oswrap.readFileByLine.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.runCommandToAddUser.assert_not_called()

if __name__ == '__main__':
	unittest.main()

