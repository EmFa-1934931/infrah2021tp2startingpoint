import unittest
import requests
import json
from codeAPI import userAPI


IP = "127.0.0.1"
PORT = "5555"
URL = "http://" + IP + ":" + PORT + "/"
#Pour le test delUser, sinon le test ne passe pas
API_KEY = '1'

class BasicTests(unittest.TestCase):

	#test route /
	def test_hello(self):
		response = requests.get(URL)
		self.assertEqual(200, response.status_code)

		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		self.assertIn('msg', obj)

	#Tests delUser
	def test_delUser(self):
		response = requests.get(URL + "adduser?username=Roy")
		self.assertEqual(200, response.status_code)

		response = requests.get(URL + "getusers?" + API_KEY)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertIn('Roy', obj['msg']['NewUsers'])

		response = requests.get(URL + "deluser?username=Roy")
		self.assertEqual(200, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])

		response = requests.get(URL + "getusers")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertNotIn('Roy', obj['msg']['NewUsers'])

	def test_delUserNoParam(self):
		response = requests.get(URL + "deluser")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1000', obj['code'])

	def test_delUserNonExisting(self):
		response = requests.get(URL + "deluser?username=Brodeur")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1002', obj['code'])

	def test_delInitialUser(self):
		response = requests.get(URL + "deluser?username=root")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1003', obj['code'])

	#Tests addUser
	def test_addUserNoParam(self):
		response = requests.get(URL + "adduser")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1000', obj['code'])

	def test_addUser(self):
		response = requests.get(URL + "adduser?username=Roy")
		self.assertEqual(200, response.status_code)

		response = requests.get(URL + "getusers")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertIn('Roy', obj['msg']['NewUsers'])

		requests.get(URL + "deluser?username=Roy")

	def test_addUserWhenUserALreadyExists(self):
		response = requests.get(URL + "adduser?username=root")
		self.assertEqual(400, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('1001', obj['code'])

	#Tests getUser
	def test_getUserIfNoNewUser(self):
		response = requests.get(URL + "getusers")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual([], obj['msg']['NewUsers'])

	def test_getUserIfNewUser(self):
		response = requests.get(URL + "adduser?username=Roy")
		self.assertEqual(200, response.status_code)
		response = requests.get(URL + "adduser?username=Matt")
		self.assertEqual(200, response.status_code)

		response = requests.get(URL + "getusers")
		obj = json.loads(response.content.decode('utf-8'))
		self.assertIn('Roy', obj['msg']['NewUsers'])
		self.assertIn('Matt', obj['msg']['NewUsers'])

		requests.get(URL + "resetusers")

	#Tests resetUsers
	def test_resetUsersIfNoNewUser(self):
		response = requests.get(URL + "resetusers")
		self.assertEqual(200, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		self.assertEqual('There were no users to delete.', obj['msg'])

	def test_resetUsersIfTwoNewUsers(self):
		response = requests.get(URL + "adduser?username=Roy")
		self.assertEqual(200, response.status_code)
		response = requests.get(URL + "adduser?username=Matt")
		self.assertEqual(200, response.status_code)
		count = 2

		response = requests.get(URL + "resetusers")
		self.assertEqual(200, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		self.assertEqual("Successfully deleted " + str(count) + " users.", obj['msg'])

	#Tests getLog
	def test_getLogs(self):
		response = requests.get(URL + "adduser?username=Roy")
		self.assertEqual(200, response.status_code)
		response = requests.get(URL + "adduser?username=Matt")
		self.assertEqual(200, response.status_code)

		response = requests.get(URL + "getlog")
		self.assertEqual(200, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		requests.get(URL + "resetusers")

	def test_getLogs(self):
		response = requests.get(URL + "adduser?username=Roy")
		self.assertEqual(200, response.status_code)
		response = requests.get(URL + "deluser?username=Roy")
		self.assertEqual(200, response.status_code)

		response = requests.get(URL + "getlog")
		self.assertEqual(200, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual('2000', obj['code'])
		requests.get(URL + "resetusers")

if __name__ == '__main__':
	unittest.main()
