#!/bin/bash

#run unittest
if coverage run -m --omit */test_*,*__init__.py,/usr/lib/python3/dist-packages/blinker/base.py unittest discover -s ./project -v -p test_*.py
then
	coverage report
	#create new container
	sh runDocker.sh

	#wait 2 seconds
	sleep 2

	#run deployment tests
	python3 -m unittest discover -s ./project -v -p dtest_*.py
else
	echo "unittest failed, aborting"
	exit 1
fi


