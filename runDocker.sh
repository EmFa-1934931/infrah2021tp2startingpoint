#!/bin/bash

#stop container
docker stop tp2ef

#delete container
docker rm tp2ef -v

#delete image and volume
docker rmi tp2ef_image
docker volume rm tp2ef_vol

#rebuild image and volume
docker volume create --name tp2ef_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp2ef_image -f ./project/docker/Dockerfile .

#Run container
service rsyslog start
docker run -p 5555:5555 --mount source=tp2ef_vol,target=/mnt/app/ --name tp2ef tp2ef_image 
#docker exec -it tp2ef bin/bash